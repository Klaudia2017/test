const A = [2,3,9,2,5,1,3,7,10];
const B = [2,1,3,4,3,10,6,6,1,7,10,10,10];
const C = [2,9,2,5,7,10];
const primeNumbers = [2,3,5,7,11,13,17];

const getFinalNumber = (A, B, primeNumbers) => {

    const result = A.filter(a => {
        const doublons = B.filter(b => b === a);
        if (doublons.length){
            const primeNumberOfDoublons = primeNumbers.findIndex(primeNumber => {
                return primeNumber === doublons.length
            });
            if( primeNumberOfDoublons === -1 ) { return true; }

            return false;
        } 
        return true;
    });
    return result;
}


console.log(getFinalNumber(A, B, primeNumbers));